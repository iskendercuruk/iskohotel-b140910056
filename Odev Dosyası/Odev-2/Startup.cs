﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Odev_2.Startup))]
namespace Odev_2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
